pushd /home/vagrant
cp /vagrant/devops-0-setup-course/Setup_localJenkins/Dockerfile .
cp /vagrant/devops-0-setup-course/Setup_localJenkins/jenkins.bash_profile .
cp /vagrant/devops-0-setup-course/Setup_localJenkins/toolchain-jenkins.xml .
sudo docker build -t localjenkins .
popd
